import React, { useState } from 'react';
import { StyleSheet, View, Button, Picker, SafeAreaView, ScrollView, Text, TextInput } from 'react-native';

export default function App() {
  const [SCALES, setTEST_SCALES] = useState([
    { identity: "I0", scale: "S0" },
    { identity: "I1", scale: "S1" },
    { identity: "I2", scale: "S2" },
    { identity: "I3", scale: "S3" },
    { identity: "I4", scale: "S4" }
  ]);

  let submitOnPressHandler = () => alert("You pressed the button " + "Submit");

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View><Text>Record Your Weight</Text></View>
          <View><Text>Requirements</Text></View>
          <View><Text>1. Before Any Eating Or Drinking</Text></View>
          <View><Text>2. After Bathroom Break</Text></View>
          <View><Text>3. Completely Naked Without Any Clothing Or Other Items</Text></View>
          <View><Text>4. A Naked Photo To Track Yourself Over Time</Text></View>
          <View>
            <Text>Select Your Scale:</Text>
            <Picker
              selectedValue={SCALES}>
            </Picker>
          </View>
          <View>
            <Text>Enter Your Weight</Text>
            <TextInput
              style={{}}
              placeholder="Enter Your Weight (Kg)"
            />
            <Text>(Kg)</Text>
          </View>
          <View style={styles.buttonContainer}><Button title="Submit" disabled={true} onPress={() => submitOnPressHandler()} /></View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  buttonContainer: {
    marginTop: 20
  }
});
